import './App.css';
//react and javascript hook modules
import React, { useState } from "react";
import ReactDOM from 'react-dom';


function App() {

    const [items, setItems] = useState([]);


    fetch("http://localhost:8080/customer/all/").then(res => res.json())
        .then( (result) => {
            setItems(result);
        })



    return (

        <ul>

            {

                items.map(item => (
                    <li key={item.id}>
                        {item.id} {item.firstName}
                    </li>
                ))

            }

        </ul>
    );
}

export default App;
