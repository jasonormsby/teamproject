package com.allstate.teamproject.entities;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;
import com.allstate.teamproject.entities.Claim;

import javax.persistence.*;

@Entity
public class Customer implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String firstName;
    private String lastName;
    private String city;
    private String zip;
    private ZonedDateTime dateJoined;



    //from claim select claim c where c.custumerid = id
    //this is causing recursive fetch
//    @OneToMany(mappedBy = "customer",cascade={CascadeType.MERGE,CascadeType.PERSIST})
//    private List<Claim> claims;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public ZonedDateTime getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(ZonedDateTime dateJoined) {
        this.dateJoined = dateJoined;
    }

//    public List<Claim> getClaims() {
//        return claims;
//    }
//
//    public void setClaims(List<Claim> claims) {
//        this.claims = claims;
//    }
}
