package com.allstate.teamproject.rest;

import com.allstate.teamproject.entities.Customer;
import com.allstate.teamproject.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.time.ZonedDateTime;
import java.util.List;

@RestController
@CrossOrigin
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = "/customer/all", method = RequestMethod.GET)
    public List<Customer> getAllCustomers(){
        return customerService.getAllCustomers();
    }

    //customer/12
    @RequestMapping(value = "/customer/get/{id}", method = RequestMethod.GET)
    public Customer getCustomerById(@PathVariable("id") int id){
        return customerService.getCustomerById(id);
    }

    //http://localhost:8080/customer/add?fName=John&lName=Doe&city=phoenix&zip=54321
    @RequestMapping(value = "/customer/add", method = RequestMethod.POST)
    public void addCustomer( @RequestParam("fName") String fName, @RequestParam("lName") String lName,
                                    @RequestParam("city") String city, @RequestParam("zip") String zip){

        customerService.addCustomer(fName,lName,city,zip);
    }

    //http://localhost:8080/customer/update?id=1fName=John&lName=Doe&city=phoenix&zip=0000
    @RequestMapping(value = "/customer/update", method = RequestMethod.PUT)
    public void updateCustomer( @RequestParam("id") int id, @RequestParam("fName") String fName, @RequestParam("lName") String lName,
                                    @RequestParam("city") String city, @RequestParam("zip") String zip){
        customerService.updateCustomer(id,fName,lName,city,zip);
    }

    //customer/12
    @RequestMapping(value = "/customer/delete/{id}", method = RequestMethod.DELETE)
    public void removeCustomerById(@PathVariable("id") int id){
        customerService.removeCustomer(id);
    }




}
