package com.allstate.teamproject.rest;

import com.allstate.teamproject.entities.Claim;
import com.allstate.teamproject.entities.Customer;
import com.allstate.teamproject.repository.ClaimsRepository;
import com.allstate.teamproject.service.ClaimsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class ClaimsController {

    @Autowired
    private ClaimsService claimsService;

    @RequestMapping(value = "/claims/get/{id}", method = RequestMethod.GET)
    public List<Claim> getClaims(@PathVariable("id") int id){
        return claimsService.getAllClaims(id);
    }

    //http://localhost:8080/claims/create?id=1&amount=100&number=12&status=done&type=house
    @RequestMapping(value = "/claims/create", method = RequestMethod.POST)
    public void createClaim(@RequestParam("id") int customerId, @RequestParam("amount") String amount,  @RequestParam("number") String number,
                                @RequestParam("status") String status,  @RequestParam("type") String type){

        claimsService.createClaim(customerId,amount,number,status,type);
    }

    @RequestMapping(value = "/claims/update", method = RequestMethod.PUT)
    public void updateClaim(@RequestParam("id") int claimId, @RequestParam("amount") String amount,  @RequestParam("number") String number,
                       @RequestParam("status") String status,  @RequestParam("type") String type){

        claimsService.updateClaim(claimId, amount, number, status, type);
    }




}
