package com.allstate.teamproject.repository;

import com.allstate.teamproject.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    public List<Customer> findAll();

    public Customer findCustomerById(int id);

    public Customer deleteById(int id);

}
