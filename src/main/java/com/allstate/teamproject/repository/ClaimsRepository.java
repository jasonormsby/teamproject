package com.allstate.teamproject.repository;

import com.allstate.teamproject.entities.Claim;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClaimsRepository extends JpaRepository<Claim,Integer> {

    public List<Claim> findAllByCustomer_Id(int id);

    public Claim findById(int id);

}
