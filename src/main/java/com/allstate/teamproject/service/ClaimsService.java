package com.allstate.teamproject.service;


import com.allstate.teamproject.entities.Claim;
import com.allstate.teamproject.entities.Customer;
import com.allstate.teamproject.repository.ClaimsRepository;
import com.allstate.teamproject.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;

@Service
public class ClaimsService {

    @Autowired
    private ClaimsRepository claimsRepository;

    @Autowired
    private CustomerRepository customerRepository;

    public List<Claim> getAllClaims(int id){



        return claimsRepository.findAllByCustomer_Id(id);
    }

    public void createClaim(int id, String amount,String number,String status,String type){
       try {
           Claim claim = new Claim();
           claim.setCustomer(customerRepository.findCustomerById(id));
           claim.setClaimNumber(Integer.parseInt(number));
           claim.setClaimAmount(Integer.parseInt(amount));
           claim.setClaimDate(ZonedDateTime.now().toString());
           claim.setClaimStatus(status);
           claim.setClaimType(type);
           claimsRepository.save(claim);
           System.out.println("claim created");
       }catch(Exception e){
           e.printStackTrace();

       }
    }

    public void updateClaim(int id, String amount, String number, String status, String type){
        Claim claim = claimsRepository.findById(id);
        claim.setClaimNumber(Integer.parseInt(number));
        claim.setClaimAmount(Integer.parseInt(amount));
        claim.setClaimDate(ZonedDateTime.now().toString());
        claim.setClaimStatus(status);
        claim.setClaimType(type);
        claimsRepository.save(claim);
    }



}
