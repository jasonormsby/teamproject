package com.allstate.teamproject.service;

import com.allstate.teamproject.entities.Customer;
import com.allstate.teamproject.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public List<Customer> getAllCustomers(){
        return customerRepository.findAll();
    }

    public void addCustomer(String fName,String lName, String city, String zip){
        Customer customer = new Customer();
        customer.setFirstName(fName);
        customer.setLastName(lName);
        customer.setCity(city);
        customer.setZip(zip);
        customer.setDateJoined(ZonedDateTime.now());
        customerRepository.save(customer);
    }

    public Customer getCustomerById(int id){
        Customer customer = customerRepository.findCustomerById(id);

        System.out.println(customer.getFirstName());


        return customer;
    }

    public void updateCustomer(int id, String fName, String lName, String city, String zip){
        Customer customer = customerRepository.findCustomerById(id);
        customer.setFirstName(fName);
        customer.setLastName(lName);
        customer.setCity(city);
        customer.setZip(zip);
        customer.setDateJoined(ZonedDateTime.now());
        customerRepository.save(customer);
    }

    public void removeCustomer(int id){
        customerRepository.deleteById(id);
    }

}
